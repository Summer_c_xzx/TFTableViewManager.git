Pod::Spec.new do |s|
  s.name         = "TFTableViewManager"
  s.version      = "2.0.0"
  s.summary      = "TableView 管理器"
  s.homepage     = "https://gitee.com/Summer_c_xzx/TFTableViewManager.git"
  s.license      = "Copyright (C) 2017 TimeFace, Inc.  All rights reserved."
  s.author             = { "Summer" => "418463162@qq.com" }
  s.social_media_url   = "https://gitee.com/Summer_c_xzx/TFTableViewManager.git"
  s.ios.deployment_target = "8.0"
  s.source       = { :git => "https://gitee.com/Summer_c_xzx/TFTableViewManager.git", :tag => s.version.to_s}
  s.source_files  = "TFTableViewManager/**/*.{h,m,c}"
  s.requires_arc = true
end
